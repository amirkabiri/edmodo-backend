<?php

use App\Models\Course;
use App\Models\Event;
use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coursesCount = Course::count();
        factory(Event::class, 10)->create()->each(function(Event $event) use($coursesCount){
            $event->course_id = rand(1, $coursesCount);
            $event->user_id = $event->course->teacher_id;
            $event->save();
        });
    }
}
