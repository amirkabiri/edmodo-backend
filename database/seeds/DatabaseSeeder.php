<?php

use App\Models\Course;
use App\Models\Event;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProvincesSeeder::class);

        $this->call(UsersTableSeeder::class);

        $this->call(CoursesTableSeeder::class);

        $this->call(EventsTableSeeder::class);
    }
}
