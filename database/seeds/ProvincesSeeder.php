<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class ProvincesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $provinces = Storage::get('provinces.json');
        $provinces = json_decode($provinces);

        foreach ($provinces as $province_name => $city_names){
            $province = \App\Models\Province::create([
                'name' => $province_name
            ]);
            foreach ($city_names as $city_name){
                $province->cities()->create([
                    'name' => $city_name
                ]);
            }
        }
    }
}
