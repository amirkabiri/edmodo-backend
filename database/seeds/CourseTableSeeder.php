<?php

use App\Models\Course;
use App\Models\User;
use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersCount = User::count();
        factory(Course::class, 3)->create()->each(function(Course $course) use($usersCount){
            $course->teacher_id = rand(1, $usersCount);
            $course->save();
        });
    }
}
