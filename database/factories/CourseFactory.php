<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Course::class, function (Faker $faker) {
    return [
        'title' => implode(' ', $faker->words),
        'description' => $faker->paragraph(5),
        'teacher_id' => 1,
        'photo' => null,
        'students_count' => 0,
        'max_students' => 0,
        'key' => uniqid(),
        'password' => null
    ];
});
