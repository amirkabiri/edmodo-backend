<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Course;
use App\Models\Project;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'course_id' => Course::inRandomOrder()->first()->id,
        'title' => implode(' ', $faker->words),
        'description' => $faker->paragraph(5),
        'deadline' => now()
    ];
});
