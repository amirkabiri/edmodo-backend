<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\City;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $result = [
        'name' => $faker->firstName .' '. $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'username' => $faker->unique()->userName,
        'email_verified_at' => now(),
        'password' => Hash::make('1234'), // password
        'last_activity' => now(),
        'phone' => $faker->phoneNumber
    ];

    $citiesCount = City::count();
    if($citiesCount > 0){
        $city_id = rand(1, $citiesCount);
        $city = City::find($city_id);

        $result['city_id'] = $city_id;
        $result['province_id'] = $city->province_id;
    }

    return $result;
});
