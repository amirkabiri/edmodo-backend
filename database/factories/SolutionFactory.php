<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Course;
use App\Models\Solution;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

$factory->define(Solution::class, function (Faker $faker) {
    return [
        'project_id' => Course::inRandomOrder()->first()->id,
        'user_id' => User::inRandomOrder()->first()->id,
        'filename' => 'solution.zip',
        'filepath' => uniqid() . '.zip'
    ];
});
