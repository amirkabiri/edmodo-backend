<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Event;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Event::class, function (Faker $faker) {
    return [
        'title' => implode(' ', $faker->words),
        'description' => $faker->paragraph(5),
        'user_id' => 1,
        'course_id' => 1
    ];
});
