<?php

return [
    'paginate_count' => 10,

    'user_photo_thumbnails' => [100, 300, 500],
    'user_photo_max_size' => 3 * 1024, // in kilobytes

    'course_photo_thumbnails' => [100, 300, 500],
    'course_photo_max_size' => 3 * 1024, // kilobytes
];