<?php

use Illuminate\Support\Facades\Route;

/*
 * Namespace : V1
 * Prefix : api/v1
 * Name : api.v1.
 */

Route::post('login', 'LoginController@handle')->name('login');
Route::post('register', 'RegisterController@handle')->name('register');

Route::get('provinces', 'ProvinceController@index')->name('provinces');

Route::middleware('auth:api')->group(function(){
    Route::apiResource('courses', 'CourseController');
    Route::apiResource('courses.events', 'EventController');
    Route::apiResource('courses.projects', 'ProjectController');
    Route::apiResource('courses.projects.solutions', 'SolutionController');

    Route::prefix('/courses/{course}')->name('courses.')->group(function(){
        Route::get('/members', 'MemberController@index')->name('members');
        Route::post('/join', 'MemberController@join')->name('join');
        Route::post('/leave', 'MemberController@leave')->name('leave');
    });

    Route::name('profile.')->prefix('profile')->group(function(){
        Route::patch('/password', 'ProfileController@updatePassword')->name('update_password');
        Route::patch('/photo', 'ProfileController@updatePhoto')->name('update_photo');
        Route::get('/photo/{size?}', 'ProfileController@showPhoto')->name('show_photo');
        Route::get('/', 'ProfileController@index')->name('index');
        Route::patch('/', 'ProfileController@update')->name('update');
    });

    Route::post('logout', 'LogoutController@handle')->name('logout');
});