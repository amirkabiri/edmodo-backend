<?php

namespace App\Policies;

use App\Models\Course;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CoursePolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return true;
    }

    public function view(User $user, Course $course)
    {
        return $course->isTeacher($user) || $course->isMember($user);
    }

    public function create(User $user)
    {
        return true;
    }

    public function update(User $user, Course $course)
    {
        return $course->isTeacher($user);
    }

    public function delete(User $user, Course $course)
    {
        return $course->isTeacher($user);
    }

    public function restore(User $user, Course $course)
    {
        //
    }

    public function forceDelete(User $user, Course $course)
    {
        //
    }

    public function join(User $user, Course $course){
        return !$course->isTeacher($user) && !$course->isMember($user);
    }
    public function leave(User $user, Course $course){
        return !$course->isTeacher($user) && $course->isMember($user);
    }
}
