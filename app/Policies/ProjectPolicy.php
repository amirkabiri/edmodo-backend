<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Project;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        //
    }

    public function view(User $user, Project $project)
    {
        return $project->course->isMember($user) || $project->course->isTeacher($user);
    }

    public function create(User $user)
    {
        //
    }

    public function update(User $user, Project $project)
    {
        return $project->course->isTeacher($user);
    }

    public function delete(User $user, Project $project)
    {
        return $project->course->isTeacher($user);
    }

    public function restore(User $user, Project $project)
    {
        //
    }

    public function forceDelete(User $user, Project $project)
    {
        //
    }
}
