<?php

namespace App\Policies;

use App\Models\Course;
use App\Models\Event;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return false;
    }

    public function view(User $user, Event $event)
    {
        return $event->course->isTeacher($user) || $event->course->isMember($user);
    }

    public function create(User $user)
    {
        return false;
    }

    public function update(User $user, Event $event)
    {
        return $event->course->isTeacher($user);
    }

    public function delete(User $user, Event $event)
    {
        return $event->course->isTeacher($user);
    }

    public function restore(User $user, Event $event)
    {
        //
    }

    public function forceDelete(User $user, Event $event)
    {
        //
    }
}
