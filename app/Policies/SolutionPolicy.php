<?php

namespace App\Policies;

use App\Models\Course;
use App\Models\Project;
use App\Models\Solution;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Log;

class SolutionPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        //
    }

    public function view(User $user, Solution $solution)
    {
        return $solution->project->course->isTeacher($user) || $solution->user_id == $user->id;
    }

    public function create(User $user, Course $course, Project $project)
    {
        return $course->isMember($user);
    }

    public function update(User $user, Solution $solution)
    {
        return $solution->user_id == $user->id;
    }

    public function delete(User $user, Solution $solution)
    {
        return $solution->user_id == $user->id;
    }

    public function restore(User $user, Solution $solution)
    {
        //
    }

    public function forceDelete(User $user, Solution $solution)
    {
        //
    }
}
