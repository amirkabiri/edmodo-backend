<?php

namespace App\Providers;

use App\Models\Course;
use App\Models\Event;
use App\Models\Project;
use App\Models\Solution;
use App\Policies\CoursePolicy;
use App\Policies\EventPolicy;
use App\Policies\ProjectPolicy;
use App\Policies\SolutionPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Course::class => CoursePolicy::class,
        Event::class => EventPolicy::class,
        Project::class => ProjectPolicy::class,
        Solution::class => SolutionPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
