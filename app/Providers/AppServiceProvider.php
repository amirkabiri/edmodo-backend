<?php

namespace App\Providers;

use App\Models\Course;
use App\Models\CourseUser;
use App\Models\Project;
use App\Models\User;
use App\Observers\CourseObserver;
use App\Observers\CourseUserObserver;
use App\Observers\ProjectObserver;
use App\Observers\UserObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    private $observers = [
        User::class => UserObserver::class,
        Course::class => CourseObserver::class,
        CourseUser::class => CourseUserObserver::class,
        Project::class => ProjectObserver::class,
    ];

    public function boot()
    {
        Schema::defaultStringLength(191);

        $this->registerObservers();
    }

    public function registerObservers(){
        /** @var Model $model */
        foreach ($this->observers as $model => $observer)
            $model::observe($observer);
    }
}
