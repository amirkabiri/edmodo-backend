<?php


namespace App\Helpers;


use App\Models\Course;
use App\Models\Project;
use App\Models\Solution;
use Illuminate\Support\Facades\Storage;

class SolutionStorage
{
    private static $path = 'solutions';

    public static function removeCourseSolutionFiles(Course $course){
        $projects = $course->projects()->with('solutions')->get();
        foreach ($projects as $project){
            foreach ($project->solutions as $solution){
                self::removeSolutionFile($solution);
            }
        }
    }
    public static function removeProjectSolutionFiles(Project $project){
        foreach ($project->solutions as $solution){
            self::removeSolutionFile($solution);
        }
    }
    public static function removeSolutionFile(Solution $solution){
        if(Storage::exists(self::$path . '/' . $solution->filepath)){
            return Storage::delete(self::$path . '/' . $solution->filepath);
        }
        return false;
    }
}