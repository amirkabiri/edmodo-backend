<?php

namespace App\Observers;

use App\Helpers\SolutionStorage;
use App\Models\Project;

class ProjectObserver
{
    public function deleting(Project $project){
        // when project removed, remove solutions files from storage
        SolutionStorage::removeProjectSolutionFiles($project);
    }
}
