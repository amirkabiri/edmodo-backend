<?php

namespace App\Observers;

use App\Helpers\SolutionStorage;
use App\Models\Course;
use Illuminate\Support\Facades\Storage;

class CourseObserver
{
    public function deleting(Course $course){
        // when course removed, remove solutions files from storage
        SolutionStorage::removeCourseSolutionFiles($course);

        // detach course members
        $course->members()->detach();
    }
}
