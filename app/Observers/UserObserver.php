<?php

namespace App\Observers;

use App\Helpers\SolutionStorage;
use App\Models\User;

class UserObserver
{
    public function deleting(User $user)
    {
        // when a user deleted, decrease students_count column of courses that user was joined in those
        foreach ($user->coursesAsStudent as $course){
            $course->students_count --;
            $course->save();
        }

        // if teacher removed, teacher courses will remove, so solution files of the course must remove
        foreach ($user->coursesAsTeacher as $course){
            SolutionStorage::removeCourseSolutionFiles($course);
        }

        // make user leave all of courses which was joined in those before
        $user->coursesAsStudent()->detach();
    }
}
