<?php

namespace App\Observers;

use App\Models\Course;
use App\Models\CourseUser;

class CourseUserObserver
{
    public function created(CourseUser $courseUser)
    {
        // when user joined in a course, update students_count column of the course
        $course = Course::find($courseUser->course_id);
        $course->students_count ++;
        $course->save();
    }

    public function deleted(CourseUser $courseUser)
    {
        // when user left a course, update students_count column of the course
        $course = Course::find($courseUser->course_id);
        $course->students_count --;
        $course->save();
    }
}
