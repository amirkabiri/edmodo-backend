<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LogoutController extends Controller
{
    public function handle()
    {
        auth()->user()->update(['api_token' => null]);

        return response()->noContent();
    }
}