<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\Teacher as TeacherResource;
use App\Http\Resources\Member as MemberResource;
use App\Models\Course;
use App\Models\User;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    // return all users paginated
    public function index(Course $course)
    {
        $this->authorize('view', $course);

        return MemberResource::collection($course->members()->paginate(config('config.paginate_count')));
    }

    public function join(Course $course){
        $this->authorize('join', $course);

        $course->members()->attach(auth()->id());

        return response(null, 204);
    }

    public function leave(Course $course){
        $this->authorize('leave', $course);

        $course->members()->detach(auth()->id());

        return response(null ,204);
    }
}
