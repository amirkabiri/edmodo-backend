<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Http\Resources\Province as ProvinceResource;

class ProvinceController extends Controller
{
    public function index(){
        $provinces = Province::with('cities')->get();

        return ProvinceResource::collection($provinces);
    }
}
