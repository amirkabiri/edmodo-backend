<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Event;
use Illuminate\Http\Request;
use App\Http\Resources\Event as EventResource;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Event::class, null, [
            'except' => ['index', 'store']
        ]);
    }

    // events of course
    public function index(Course $course)
    {
        $this->authorize('view', $course);

        return EventResource::collection($course->events);
    }

    // create new event
    public function store(Course $course, Request $request)
    {
        $this->authorize('update', $course);

        $validation = Validator::make($request->all(), [
            'title' => 'required',
//            'description' => '',
        ]);
        if($validation->fails()) return response()->json($validation->errors(), 422);

        $event = Event::create([
            'title' => $request->title,
            'description' => $request->get('description', null),
            'course_id' => $course->id,
            'user_id' => auth()->id()
        ]);

        return new EventResource($event);
    }

    // show event
    public function show(Course $course, Event $event)
    {

        $event = $course->events()->findOrFail($event->id);

        return new EventResource($event);
    }


    // update event
    public function update(Course $course, Request $request, Event $event)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required',
//            'description' => 'required'
        ]);
        if($validation->fails()) return response()->json($validation->errors(), 422);

        $event = $course->events()->findOrFail($event->id);
        $event->title = $request->title;
        $event->description = $request->get('description', null);
        $event->save();

        return new EventResource($event);
    }

    // remove event
    public function destroy(Course $course, Event $event)
    {
        $event = $course->events()->findOrFail($event->id);
        $event->delete();

        // todo remove solution files
        
        return response(null, 204);
    }
}
