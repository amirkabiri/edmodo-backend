<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\User as UserResource;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{
    public function updatePassword(Request $request){
        $validation = Validator::make($request->all(), [
            'password' => 'required'
        ]);
        if($validation->fails()) return response()->json($validation->errors(), 422);

        auth()->user()->update([
            'password' => Hash::make($request->password)
        ]);
        return response()->noContent();
    }

    public function update(Request $request){
        $user = auth()->user();

        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'username' => "unique:users,username,$user->id|min:5",
            'phone' => 'iran_mobile',
            'national_id' => 'melli_code',
            'city_id' => 'exists:cities,id',
        ]);
        if($validation->fails()) return response()->json($validation->errors(), 422);

        $data = [];
        foreach (['name', 'username', 'phone', 'national_id', 'city_id'] as $key){
            if(!$request->has($key)) continue;

            if($key == 'phone'){
                $data[$key] = $this->normalizePhone($request->input($key));
            }else{
                $data[$key] = $request->input($key);
            }
        }
        $user->update($data);

        return new UserResource($user);
    }

    public function normalizePhone($phone){
        if(substr($phone, 0, 1) == '0'){
            return '+98' . substr($phone, 1);
        }
        if(substr($phone, 0, 2) == '98'){
            return '+' . $phone;
        }
        if(substr($phone, 0, 1) == '9'){
            return '+98' . $phone;
        }
        return $phone;
    }

    public function updatePhoto(Request $request){
        $validation = Validator::make($request->all(), [
            'photo' => 'required|file|mimes:png,jpg,jpeg|max:'. config('config.user_photo_max_size')
        ]);
        if($validation->fails()) return response()->json($validation->errors(), 422);

        $user = auth()->user();
        $photo = $request->file('photo');
        $name = $user->id .'-'. time() .'-'. uniqid() .'.'. $photo->getClientOriginalExtension();

        // check if user already have a photo, remove that photo from storage
        if($user->photo) {
            foreach (config('config.user_photo_thumbnails') as $size){
                $path = 'avatars/' . $size . 'x' . $size .'/'. $user->photo;

                if(!Storage::exists($path)) continue;

                Storage::delete($path);
            }
            if(Storage::exists('avatars/' . $user->photo)){
                Storage::delete('avatars/'.$user->photo);
            }
        }

        $photo->storeAs('avatars', $name);
        $this->makeThumbnails($name);
        $user->update(['photo' => $name]);

        // todo updatePhoto@ProfileController should not return photo name
        return $name;
    }

    public function index(){
        $user = auth()->user();
        return new UserResource($user);
    }

    public function showPhoto($size = null){
        $user = auth()->user();
        $path = 'avatars/';

        // if user has not photo
        if(is_null($user->photo)){
            return response(null, 404);
        }

        if(is_null($size)){
            $path .= $user->photo;
        }else{
            $path .= "{$size}x{$size}/" . $user->photo;
        }

        if(!Storage::exists($path)) return response(null, 404);

        return response()->file(storage_path('app/' . $path));
    }

    public function makeThumbnails($name){
        $sizes = config('config.user_photo_thumbnails');
        $path = storage_path('app/avatars/');

        foreach ($sizes as $size){
            $folder = 'avatars/' . $size . 'x' . $size;
            if(!Storage::exists($folder)){
                Storage::makeDirectory($folder);
            }

            $image = Image::make($path . $name);
            $image->fit($size, $size, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image->save($path ."{$size}x{$size}/". $name);
        }
    }
}
