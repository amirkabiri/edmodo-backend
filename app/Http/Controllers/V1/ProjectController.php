<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Resources\Project as ProjectResource;
use Illuminate\Support\Facades\Validator;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Project::class, null, [
            'except' => ['index', 'store']
        ]);
    }

    public function index(Course $course)
    {
        $this->authorize('view', $course);

        return ProjectResource::collection($course->projects);
    }

    public function store(Course $course, Request $request)
    {
        $this->authorize('update', $course);

        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'deadline' => 'nullable',
            'description' => 'nullable',
        ]);
        if($validation->fails()) return response()->json($validation->errors(), 422);

        $project = Project::create([
            'title' => $request->title,
            'deadline' => $request->get('deadline', null),
            'description' => $request->get('description', null),
            'course_id' => $course->id,
        ]);

        return new ProjectResource($project);
    }


    public function show(Course $course, Project $project)
    {
        return new ProjectResource($project->withoutRelations());
    }

    public function update(Course $course, Request $request, Project $project)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'deadline' => 'nullable',
            'description' => 'nullable'
        ]);
        if($validation->fails()) return response()->json($validation->errors(), 422);

        if($request->has('description')){
            $project->description = $request->get('description', null);
        }
        if($request->has('deadline')){
            $project->deadline = $request->get('deadline', null);
        }
        $project->title = $request->title;
        $project->save();

        return new ProjectResource($project->withoutRelations());
    }

    public function destroy(Course $course, Project $project)
    {
        $project->delete();

        // todo remove solution files

        return response(null, 204);
    }
}
