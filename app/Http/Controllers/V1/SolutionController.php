<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Project;
use App\Models\Solution;
use Illuminate\Http\Request;
use App\Http\Resources\Solution as SolutionResource;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class SolutionController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Solution::class, null, [
            'except' => ['index', 'store']
        ]);
    }

    public function index(Course $course, Project $project)
    {
        $this->authorize('view', $course);

        $user = auth()->user();

        if($course->isTeacher($user)){
            return SolutionResource::collection($project->solutions);
        }

        if($course->isMember($user)){
            $solutions = Solution::where('user_id', $user->id)->where('project_id', $project->id)->get();
            return SolutionResource::collection($solutions);
        }
    }

    public function store(Course $course, Project $project, Request $request)
    {
        $this->authorize('create', [Solution::class, $course, $project]);

        $validation = Validator::make($request->all(), [
            'file' => 'required|file|mimes:zip,rar|max:'. (10 * 1024)
        ]);
        if($validation->fails()) return response()->json($validation->errors(), 422);

        $file = $request->file('file');
        $filepath = $this->uploadFile($file);

        $solution = $project->solutions()->create([
            'user_id' => auth()->id(),
            'filename' => $file->getClientOriginalName(),
            'filepath' => $filepath
        ]);

        return new SolutionResource($solution->withoutRelations());
    }

    public function show(Course $course, Project $project, Solution $solution)
    {
        return new SolutionResource($solution->withoutRelations());
    }

    public function update(Course $course, Project $project, Solution $solution, Request $request)
    {
        $validation = Validator::make($request->all(), [
            'file' => 'required|file|mimes:zip,rar|max:'. (10 * 1024)
        ]);
        if($validation->fails()) return response()->json($validation->errors(), 422);

        $this->removeCurrentFile($solution);

        $file = $request->file('file');
        $filepath = $this->uploadFile($file);

        $solution->update([
            'filename' => $file->getClientOriginalName(),
            'filepath' => $filepath
        ]);

        return new SolutionResource($solution->withoutRelations());
    }

    public function destroy(Course $course, Project $project, Solution $solution)
    {
       $this->removeCurrentFile($solution);

        $solution->delete();

        return response()->noContent();
    }

    private function uploadFile($file){
        $filepath = auth()->id() .'-'. time() .'-'. uniqid() .'.'. $file->getClientOriginalExtension();
        $file->storeAs('solutions', $filepath);
        return $filepath;
    }
    private function removeCurrentFile(Solution $solution){
        $file = 'solutions/' . $solution->filepath;
        if(Storage::exists($file)){
            return Storage::delete($file);
        }
        return false;
    }
}
