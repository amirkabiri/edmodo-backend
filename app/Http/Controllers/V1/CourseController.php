<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;
use App\Http\Resources\Course as CourseResource;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use phpDocumentor\Reflection\DocBlock\Tags\Author;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Course::class);
    }

    // show all courses of user
    public function index()
    {
        $user = auth()->user();

        return [
            'teacher' => CourseResource::collection($user->coursesAsTeacher()->with('teacher')->get()),
            'student' => CourseResource::collection($user->coursesAsStudent()->with('teacher')->get())
        ];
    }

    // create new course
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'photo' => 'file|mimes:png,jpg,jpeg|max:'. config('config.course_photo_max_size')
        ]);
        if($validation->fails()) return response()->json($validation->errors(), 422);

        $user = auth()->user();
        $data = $request->only('title', 'description');

        if($request->hasFile('photo')){
            $photo = $request->file('photo');
            $photoName = $user->id .'-'. time() .'-'. uniqid() .'.'. $photo->getClientOriginalExtension();
            $data['photo'] = $photoName;

            $photo->storeAs('courses', $photoName);
            $this->makeThumbnails($photoName);
        }

        $course = $user->coursesAsTeacher()->create($data);
        return (
            new CourseResource(Course::with('teacher')->find($course->id))
        )->response()->setStatusCode(201);
    }

    // get course by id
    public function show(Course $course)
    {
        return new CourseResource(Course::with('teacher')->findOrFail($course->id));
    }

    // update course by id
    public function update(Request $request, Course $course)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required'
        ]);
        if($validation->fails()) return response()->json($validation->errors(), 422);

        $course->title = $request->title;
        $course->description = $request->description;
        $course->save();

        return new CourseResource(Course::with('teacher')->find($course->id));
    }

    // delete course by id
    public function destroy(Course $course)
    {
        // TODO remove course photos
        // TODO remove attachment files
        // TODO remove course solution files
        $course->delete();

        return response(null, 204);
    }

    public function removeCoursePhoto(Course $course){

    }

    public function makeThumbnails($name){
        $sizes = config('config.course_photo_thumbnails');
        $path = storage_path('app/courses/');

        foreach ($sizes as $size){
            $folder = 'courses/' . $size . 'x' . $size;
            if(!Storage::exists($folder)){
                Storage::makeDirectory($folder);
            }

            $image = Image::make($path . $name);
            $image->fit($size, $size, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image->save($path ."{$size}x{$size}/". $name);
        }
    }
}
