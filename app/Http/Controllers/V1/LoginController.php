<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    public function handle(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
            'password' => 'required'
        ]);
        if ($validation->fails()) return response()->json($validation->errors(), 422);

        if (!Auth::once($request->only('email', 'password'))) return response()->json([
            'password' => ['password is wrong']
        ], 422);

        $token = Str::random(100);
        User::where('email', $request->email)->update(['api_token' => $token]);

        return $token;
    }
}
