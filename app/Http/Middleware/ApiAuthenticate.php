<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;

class ApiAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->hasHeader('Authorization')) return response(null, 403);

        $user = User::where('api_token', $request->header('Authorization'))->first();
        if(!$user) return response(null, 403);

        auth()->login($user);

        return $next($request);
    }
}
