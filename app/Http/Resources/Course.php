<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Teacher as TeacherResource;
use App\Http\Resources\User as UserResource;

class Course extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);

        if(isset($data['teacher'])) $data['teacher'] = new TeacherResource($data['teacher']);
        if(isset($data['members'])) $data['members'] = UserResource::collection($data['members']);

        return $data;
    }
}
