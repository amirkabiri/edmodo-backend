<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Solution extends JsonResource
{
    public function toArray($request)
    {
        $output = parent::toArray($request);

        unset($output['filepath']);

        return $output;
    }
}
