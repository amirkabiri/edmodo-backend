<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['title', 'description', 'teacher_id', 'key', 'max_students', 'password', 'photo', 'students_count'];
    protected $hidden = ['password'];
    protected $casts = [
        'max_students' => 'integer',
        'teacher_id' => 'integer',
        'students_count' => 'integer'
    ];

    // TODO test setKey attribute work when key not set
    public function setKeyAttribute($value = null){
        if(empty($value)) return uniqid();

        return $value;
    }

    /*
     * relation with User::class model
     */
    public function members(){
        return $this->belongsToMany(User::class)->using(CourseUser::class);
    }
    public function teacher(){
        return $this->belongsTo(User::class, 'teacher_id', 'id');
    }

    /*
     * relation with Event::class model
     */
    public function events(){
        return $this->hasMany(Event::class, 'course_id', 'id');
    }

    /*
     * relation with Project::class model
     */
    public function projects(){
        return $this->hasMany(Project::class, 'course_id', 'id');
    }


    // my methods
    public function isMember(User $user){
        return $this->members()->newPivotStatementForId($user->id)->exists();
    }
    public function isTeacher(User $user){
        return $this->teacher_id == $user->id;
    }
}
