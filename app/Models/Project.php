<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['course_id', 'title', 'description', 'deadline'];

    protected $casts = [
        'course_id' => 'integer',
        'deadline' => 'datetime',
    ];

    protected $attributes = [
        'deadline' => null,
        'description' => null,
    ];

    public function course(){
        return $this->belongsTo(Course::class, 'course_id', 'id');
    }
    public function events(){
        return $this->hasMany(Event::class, 'project_id', 'id');
    }
    public function solutions(){
        return $this->hasMany(Solution::class, 'project_id', 'id');
    }
}
