<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Solution extends Model
{
    protected $fillable = ['project_id', 'user_id', 'filename', 'filepath'];

    protected $casts = [
        'project_id' => 'integer',
        'user_id' => 'integer',
    ];

    /*
     * relation with User::class model
     */
    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /*
     * relation with Project::class model
     */
    public function project(){
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }
}
