<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $casts = [
        'comments_count' => 'integer',
        'likes_count' => 'integer',
        'course_id' => 'integer',
        'user_id' => 'integer'
    ];

    protected $fillable = ['user_id', 'course_id', 'project_id', 'title', 'description', 'likes_count', 'comments_count'];

    protected $attributes = [
        'comments_count' => 0,
        'likes_count' => 0,
        'description' => null,
        'project_id' => null
    ];

    public function getProjectIdAttribute($project_id){
        return is_null($project_id) ? $project_id : (int) $project_id;
    }

    public function course(){
        return $this->belongsTo(Course::class, 'course_id', 'id');
    }
    public function sender(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function project(){
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }
}
