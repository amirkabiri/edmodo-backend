<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'city_id', 'province_id', 'username', 'photo', 'phone', 'national_id', 'api_token'
    ];
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setCityIdAttribute($city_id){
        $this->attributes['city_id'] = $city_id;
        $this->attributes['province_id'] = City::find($city_id)->province_id;
    }

    public function solutions(){
        return $this->hasMany(Solution::class, 'user_id', 'id');
    }

    public function sentEvents(){
        return $this->hasMany(Event::class, 'user_id', 'id');
    }
    public function receivedEvents(){
        //return $this->hasMany()
    }

    public function coursesAsStudent(){
        return $this->belongsToMany(Course::class)->using(CourseUser::class);
    }
    public function coursesAsTeacher(){
        return $this->hasMany(Course::class, 'teacher_id', 'id');
    }

    public function city(){
        return $this->belongsTo(City::class, 'city_id', 'id');
    }
    public function province(){
        return $this->belongsTo(Province::class, 'province_id', 'id');
    }
    public static function withAddress(){
        return User::with(['city:id,name', 'province:id,name']);
    }
    public function normalizeAddress(){
        $user = $this->toArray();

        if(isset($user['province'])){
            $user['province'] = $user['province']['name'];
        }
        if(isset($user['city'])){
            $user['city'] = $user['city']['name'];
        }

        return $user;
    }
}