<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Province extends Model
{
    protected $fillable = ['name'];

    public function cities(){
        return $this->hasMany(City::class, 'province_id', 'id');
    }
    public function users(){
        return $this->hasMany(User::class, 'province_id', 'id');
    }
}