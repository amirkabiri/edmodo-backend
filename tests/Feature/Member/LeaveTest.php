<?php

namespace Tests\Feature\Member;

use App\Models\Course;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LeaveTest extends Base
{
    use RefreshDatabase;

    public function test_status_with_a_user_is_not_joined_before(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $user = factory(User::class)->create();

        $response = $this->request([
            'POST',
            $this->route('leave', ['course' => $course->id])
        ], $user);

        $response->assertStatus(403);
    }
    public function test_status_with_teacher(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->request([
            'POST',
            $this->route('leave', ['course' => $course->id])
        ], $teacher);

        $response->assertStatus(403);
    }
    public function test_success_status(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $member = $course->members()->save(factory(User::class)->make());

        $response = $this->request([
            'POST',
            $this->route('leave', ['course' => $course->id])
        ], $member);

        $response->assertStatus(204);
    }
    public function test_user_left_in_db(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $member = $course->members()->save(factory(User::class)->make());

        $this->request([
            'POST',
            $this->route('leave', ['course' => $course->id])
        ], $member);

        $this->assertFalse($course->isMember($member));
    }
}
