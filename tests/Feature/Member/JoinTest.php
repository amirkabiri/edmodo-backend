<?php

namespace Tests\Feature\Member;

use App\Http\Resources\Member as MemberResource;
use App\Models\Course;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class JoinTest extends Base
{
    use RefreshDatabase;

    public function test_status_with_member(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $student = $course->members()->save(factory(User::class)->make());

        $response = $this->request([
            'POST',
            $this->route('join', ['course' => $course->id])
        ], $student);

        $response->assertStatus(403);
    }
    public function test_status_with_teacher(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $student = $course->members()->save(factory(User::class)->make());

        $response = $this->request([
            'POST',
            $this->route('join', ['course' => $course->id])
        ], $teacher);

        $response->assertStatus(403);
    }
    public function test_success_status(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $user = factory(User::class)->create();

        $response = $this->request([
            'POST',
            $this->route('join', ['course' => $course->id])
        ], $user);

        $response->assertStatus(204);
    }
    public function test_user_joined_in_db(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $user = factory(User::class)->create();

        $this->request([
            'POST',
            $this->route('join', ['course' => $course->id])
        ], $user);

        $this->assertTrue($course->isMember($user));
    }
}
