<?php

namespace Tests\Feature\Member;

use App\Models\Course;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Resources\Member as MemberResource;

class IndexTest extends Base
{
    use RefreshDatabase;

    public function test_status_with_invalid_user(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->request([
            'GET',
            $this->route('members', ['course' => $course->id])
        ]);
        $response->assertStatus(403);
    }
    public function test_status_with_teacher(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->request([
            'GET',
            $this->route('members', ['course' => $course->id])
        ], $teacher);
        $response->assertStatus(200);
    }
    public function test_status_with_student(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $student = $course->members()->save(factory(User::class)->make());

        $response = $this->request([
            'GET',
            $this->route('members', ['course' => $course->id])
        ], $student);
        $response->assertStatus(200);
    }
    public function test_structure(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $student = $course->members()->save(factory(User::class)->make());

        $response = $this->request([
            'GET',
            $this->route('members', ['course' => $course->id])
        ], $student);

        $response->assertJson([
            'data' => MemberResource::collection($course->members)->resolve()
        ]);
    }
}
