<?php

namespace Tests\Feature\Course;

use App\Http\Resources\Course as CourseResource;
use App\Models\Course;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class StoreTest extends Base
{
    use RefreshDatabase;

    public function test_store_without_data(){
        $response = $this->request(['POST', $this->route('store')]);

        $response->assertStatus(422)->assertJsonStructure(['description', 'title']);
    }
    public function test_store_with_title(){
        $response = $this->request(['POST', $this->route('store'), [
            'title' => 'this is a valid title'
        ]]);

        $response->assertStatus(422)->assertJsonStructure(['description']);
    }
    public function test_store_with_description(){
        $response = $this->request(['POST', $this->route('store'), [
            'description' => 'this is a valid description'
        ]]);

        $response->assertStatus(422)->assertJsonStructure(['title']);
    }
    public function test_image_is_file(){
        $this->request(['POST', $this->route('store'), ['photo' => 'fdsafsa']])
            ->assertStatus(422)
            ->assertJsonStructure(['photo']);

        $photo = UploadedFile::fake()->image('file.png', 2000, 2000);
        $res = $this->request(['POST', $this->route('store'), compact('photo')]);

        $this->assertArrayNotHasKey('photo', $res->json());
    }
    public function test_image_format_is_valid(){
        $rules = [
            'png' => true,
            'jpeg' => true,
            'jpg' => true,

            'mp4' => false,
            'zip' => false,
            'php' => false,
        ];
        foreach ($rules as $format => $ok){
            $photo = UploadedFile::fake()->create('file.'.$format);
            $res = $this->request(['POST', $this->route('store'), compact('photo')]);

            if($ok){
                $this->assertArrayNotHasKey('photo', $res->json());
            }else{
                $this->assertArrayHasKey('photo', $res->json());
            }
        }
    }
    public function test_image_size_is_valid(){
        $photo = UploadedFile::fake()->create('file.png', config('config.course_photo_max_size') + 1);
        $res = $this->request(['POST', $this->route('store'), compact('photo')])->json();
        $this->assertArrayHasKey('photo', $res);

        $photo = UploadedFile::fake()->create('file.png', config('config.course_photo_max_size') - 1);
        $res = $this->request(['POST', $this->route('store'), compact('photo')])->json();
        $this->assertArrayNotHasKey('photo', $res);
    }
    public function test_photo_is_optional(){
        $res = $this->request(['POST', $this->route('store')])->json();
        $this->assertArrayNotHasKey('photo', $res);
    }
    public function test_store_with_valid_data(){
        $course = factory(Course::class)->make();
        $photo = UploadedFile::fake()->image('file.png', 2000, 2000);
        $res = $this->request(['POST', $this->route('store'), [
            'description' => $course->description,
            'title' => $course->title,
            'photo' => $photo
        ]]);

        $course = Course::with('teacher')->where('title', $course->title)->where('description', $course->description)->first();
        $this->assertNotNull($course);

        $this->addCoursePhotoToDeleteQueue($course);

        $res->assertStatus(201)->assertExactJson([
            'data' => (new CourseResource(
                array_merge($course->toArray(), [
                    'title' => $course->title,
                    'description' => $course->description,
                ])
            ))->resolve()
        ]);
    }

    public function test_description_saved_correctly(){
        $course = factory(Course::class)->make();

        $res = $this->request(['POST', $this->route('store'), [
            'description' => $course->description,
            'title' => $course->title,
        ]])->json();

        $course = Course::find($res['data']['id']);
        $this->assertEquals($course->description, $course->description);
        $this->assertEquals($course->description, $res['data']['description']);
    }

    public function test_title_saved_correctly(){
        $course = factory(Course::class)->make();

        $res = $this->request(['POST', $this->route('store'), [
            'description' => $course->description,
            'title' => $course->title,
        ]])->json();

        $course = Course::find($res['data']['id']);
        $this->assertEquals($course->title, $course->title);
        $this->assertEquals($course->title, $res['data']['title']);
    }

    public function test_photo_exists_in_storage(){
        $course = factory(Course::class)->make();
        $photo = UploadedFile::fake()->image('file.png', 2000, 2000);
        $res = $this->request(['POST', $this->route('store'), [
            'description' => $course->description,
            'title' => $course->title,
            'photo' => $photo,
        ]])->json('data');

        $this->addCoursePhotoToDeleteQueue(Course::find($res['id']));

        $this->assertNotNull($res['photo']);

        Storage::assertExists('courses/' . $res['photo']);
    }

    public function test_photo_thumbnails_exists_in_storage(){
        $course = factory(Course::class)->make();
        $photo = UploadedFile::fake()->image('file.png', 2000, 2000);
        $res = $this->request(['POST', $this->route('store'), [
            'description' => $course->description,
            'title' => $course->title,
            'photo' => $photo,
        ]])->json('data');

        $this->addCoursePhotoToDeleteQueue(Course::find($res['id']));

        $this->assertNotNull($res['photo']);

        foreach (config('config.course_photo_thumbnails') as $size) {
            Storage::assertExists("courses/{$size}x{$size}/{$res['photo']}");
        }
    }
}