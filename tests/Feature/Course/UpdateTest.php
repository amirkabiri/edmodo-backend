<?php

namespace Tests\Feature\Course;

use App\Http\Resources\Course as CourseResource;
use App\Models\Course;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class UpdateTest extends Base
{
    use RefreshDatabase;

    public function test_update_with_invalid_id(){
        $response = $this->request(['PUT', $this->route('update', ['course' => 'invalid id'])]);

        $response->assertStatus(404);
    }
    public function test_update_with_invalid_teacher_id(){
        $user = factory(User::class)->create();
        $course = $user->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->actingAs(factory(User::class)->create(), 'api')->withHeaders($this->headers)->put(
            route($this->name . 'update', ['course' => $course->id])
        );

        $response->assertStatus(403);
    }
    public function test_update_with_invalid_data(){
        $user = factory(User::class)->create();
        $course = $user->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->actingAs($user, 'api')
            ->withHeaders($this->headers)
            ->put(route($this->name . 'update', ['course' => $course->id]));

        $response->assertStatus(422)->assertJsonStructure(['title', 'description']);
    }
    public function test_update_with_invalid_title(){
        $user = factory(User::class)->create();
        $course = $user->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->actingAs($user, 'api')
            ->withHeaders($this->headers)
            ->put(route($this->name . 'update', ['course' => $course->id]), [
                'title' => '',
                'description' => 'this is a valid description'
            ]);

        $response->assertStatus(422)->assertJsonStructure(['title']);
    }
    public function test_update_with_invalid_description(){
        $user = factory(User::class)->create();
        $course = $user->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->actingAs($user, 'api')
            ->withHeaders($this->headers)
            ->put(route($this->name . 'update', ['course' => $course->id]), [
                'description' => '',
                'title' => 'this is a valid title'
            ]);

        $response->assertStatus(422)->assertJsonStructure(['description']);
    }
    public function test_update_with_valid_data()
    {
        $user = factory(User::class)->create();
        $course = $user->coursesAsTeacher()->save(factory(Course::class)->make());

        $data = (new CourseResource(Course::with('teacher')->find($course->id)))->resolve();
        $data['title'] = 'this is valid description';
        $data['description'] = 'this is valid description';

        $response = $this->actingAs($user, 'api')
            ->withHeaders($this->headers)
            ->put(
                route($this->name . 'update', ['course' => $course->id]),
                [
                    'description' => $data['description'],
                    'title' => $data['title']
                ]
            );

        $response->assertStatus(200)->assertJsonStructure(['data'])->assertExactJson([
            'data' => $data
        ]);
    }
}
