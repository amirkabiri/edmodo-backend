<?php

namespace Tests\Feature\Course;

use App\Models\Course;
use App\Models\Project;
use App\Models\Solution;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class DestroyTest extends Base
{
    use RefreshDatabase;

    public function test_destroy_with_invalid_id(){
        $res = $this->request([
            'DELETE', $this->route('destroy', ['course' => 'invalid id'])
        ]);

        $res->assertStatus(404);
    }
    public function test_destroy_with_invalid_teacher_id(){
        $user = factory(User::class)->create();
        $course = $user->coursesAsTeacher()->save(factory(Course::class)->make());

        $res = $this->request([
            'DELETE', $this->route('destroy', ['course' => $course->id])
        ], factory(User::class)->create());

        $res->assertStatus(403);
    }
    public function test_destroy_with_valid_data(){
        $user = factory(User::class)->create();
        $course = $user->coursesAsTeacher()->save(factory(Course::class)->make());

        $res = $this->request([
            'DELETE', $this->route('destroy', ['course' => $course->id])
        ], $user);

        $res->assertStatus(204);
    }
    public function test_solution_files_of_course_will_remove_successfully(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $member = $course->members()->save(factory(User::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());
        $file = UploadedFile::fake()->create('file.zip');

        $res = $this->request([
            'POST',
            $this->route('projects.solutions.store', ['course' => $course->id, 'project' => $project->id]),
            compact('file')
        ], $member);
        $res->assertCreated();
        $solution = Solution::find($res->json()['data']['id']);
        Storage::assertExists('solutions/' . $solution->filepath);

        $this->request([
            'DELETE', $this->route('destroy', ['course' => $course->id])
        ], $teacher)->assertNoContent();

        Storage::assertMissing('solutions/' . $solution->filepath);
    }
}
