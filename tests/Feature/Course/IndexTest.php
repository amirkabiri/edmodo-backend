<?php

namespace Tests\Feature\Course;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class IndexTest extends Base
{
    use RefreshDatabase;

    public function test_index_status_code_is_200()
    {
        $response = $this->request(['GET', $this->route('index')]);

        $response->assertStatus(200);
    }
    public function test_index_output_structure(){
        $response = $this->request(['GET', $this->route('index')]);

        $response->assertJsonStructure(['teacher', 'student']);
    }
}
