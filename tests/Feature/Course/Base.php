<?php

namespace Tests\Feature\Course;
use App\Models\Course;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class Base extends TestCase
{
    protected $name = 'api.v1.courses.';
    protected $headers = [
        'Accept' => 'application/json'
    ];
    protected $coursePhotos = [];
    protected function addCoursePhotoToDeleteQueue(Course $course){
        $course = $course->refresh();
        $this->coursePhotos[] = $course->photo;
    }


    protected function tearDown(): void
    {
        foreach ($this->coursePhotos as $coursePhoto){
            if(Storage::exists("courses/$coursePhoto")) {
                Storage::delete("courses/$coursePhoto");
            }

            foreach (config('config.course_photo_thumbnails') as $size){
                if(Storage::exists("courses/{$size}x{$size}/$coursePhoto")) {
                    Storage::delete("courses/{$size}x{$size}/$coursePhoto");
                }
            }
        }

        parent::tearDown();
    }


    protected function route($name, $params = []){
        return route($this->name . $name, $params);
    }
    protected function request($arg1, $user = null){
        if(count($arg1) === 2) $arg1[] = [];
        list($method, $route, $params) = $arg1;

        if($user === null) $user = factory(User::class)->create();

        $response = $this->actingAs($user, 'api')->withHeaders($this->headers)->json(
            $method,
            $route,
            $params
        );

        return $response;
    }
}