<?php

namespace Tests\Feature\Course;

use App\Http\Resources\Course as CourseResource;
use App\Models\Course;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class ShowTest extends Base
{
    use RefreshDatabase;

    public function test_status_with_invalid_course_id(){
        $response = $this->request(['GET', $this->route('show', ['course' => 'invalid id'])]);

        $response->assertStatus(404);
    }
    public function test_status_with_a_user_as_teacher(){
        $user = factory(User::class)->create();
        $course = $user->coursesAsTeacher()->save(factory(Course::class)->make());
        $response = $this->request([
            'GET',
            $this->route('show', ['course' => $course->id])
        ], $user);

        $response->assertStatus(200);
    }
    public function test_status_with_a_user_as_member(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $member = $course->members()->save(factory(User::class)->make());

        $response = $this->request([
            'GET',
            $this->route('show', ['course' => $course->id])
        ], $member);

        $response->assertStatus(200);
    }
    public function test_status_with_a_invalid_user(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $invalid_user = factory(User::class)->create();

        $response = $this->request([
            'GET',
            $this->route('show', ['course' => $course->id])
        ], $invalid_user);

        $response->assertStatus(403);
    }
    public function test_structure_with_valid_data(){
        $user = factory(User::class)->create();
        $course = $user->coursesAsTeacher()->save(factory(Course::class)->make());
        $response = $this->request([
            'GET',
            $this->route('show', ['course' => $course->id])
        ], $user);

        $response->assertStatus(200)->assertExactJson([
            'data' => (new CourseResource(Course::with('teacher')->findOrFail($course->id)))->resolve()
        ]);
    }
}
