<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Feature\Profile\Base;
use App\Http\Resources\User as UserResource;

class IndexTest extends Base
{
    use RefreshDatabase;

    public function test_user_get_me_with_unauthenticated_user(){
        $this->request([
            'GET',
            $this->route('index')
        ], false)->assertUnauthorized();
    }
    public function test_user_get_me_returns_correct_data(){
        $user = $this->createUser();

        $this->request([
            'GET',
            $this->route('index'),
        ], $user)->assertOk()->assertExactJson([
            'data' => (new UserResource($user))->resolve()
        ]);
    }
}
