<?php

namespace Tests\Feature;

use App\Models\City;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use ProvincesSeeder;
use Tests\Feature\Profile\Base;
use App\Http\Resources\User as UserResource;

class UpdateTest extends Base
{
    use RefreshDatabase;

    public function test_with_unauthenticated_user(){
        $res = $this->request([
            'PATCH',
            $this->route('update')
        ], false);

        $res->assertStatus(401);
    }
    public function test_with_no_data(){
        $res = $this->request([
            'PATCH',
            $this->route('update')
        ]);

        $res->assertStatus(422)->assertJsonStructure(['name']);
    }
    public function test_name_updated(){
        $name = 'hello world name';
        $user = $this->createUser();

        $res = $this->request([
            'PATCH',
            $this->route('update'),
            compact('name')
        ], $user);

        $res->assertStatus(200)->assertExactJson([
            'data' => $user->toArray()
        ]);
    }

    public function test_username_has_length_greater_than_or_equals_5(){
        $lengths = [
            5 => true,
            6 => true,
            4 => false
        ];

        foreach ($lengths as $length => $validation_rule){
            $res = $res = $this->request([
                'PATCH',
                $this->route('update'),[
                    'username' => substr(uniqid(), 0, $length),
                    'name' => 'this is valid'
                ]
            ]);
            $res->assertStatus($validation_rule ? 200 : 422);
            if(!$validation_rule) $res->assertJsonStructure(['username']);
        }
    }
    public function test_username_is_unique(){
        $username = 'the_username';

        $res = $res = $this->request([
            'PATCH',
            $this->route('update'),[
                'username' => $username,
                'name' => 'dfd'
            ]
        ]);
        $res->assertStatus(200);

        $res = $res = $this->request([
            'PATCH',
            $this->route('update'),[
                'username' => $username,
                'name' => 'dfd'
            ]
        ]);
        $res->assertStatus(422)->assertJsonStructure(['username']);
    }
    public function test_username_updated(){
        $username = 'the_username';
        $user = $this->createUser();

        $res = $res = $this->request([
            'PATCH',
            $this->route('update'),[
                'username' => $username,
                'name' => $user->name
            ]
        ], $user);

        $res->assertStatus(200)->assertExactJson([
            'data' => $user->toArray()
        ]);
    }
    public function test_username_update_with_old_data_dont_raise_unique_error(){
        $user = $this->createUser();
        $username = 'the_username';
        $name = $user->name;

        $this->request([
            'PATCH',
            $this->route('update'),
            compact('username', 'name')
        ], $user)->assertOk();

        $this->request([
            'PATCH',
            $this->route('update'),
            compact('username', 'name')
        ], $user)->assertOk();
    }

    public function test_city_id_is_valid(){
        $this->seed(ProvincesSeeder::class);
        $city = City::inRandomOrder()->first();

        $res = $res = $this->request([
            'PATCH',
            $this->route('update'),[
                'name' => 'fdsfsd',
                'city_id' => $city->id
            ]
        ]);
        $res->assertStatus(200);

        $res = $res = $this->request([
            'PATCH',
            $this->route('update'),[
                'name' => 'fdsfsd',
                'city_id' => City::count() + 1
            ]
        ]);
        $res->assertStatus(422)->assertJsonStructure(['city_id']);
    }
    public function test_city_id_updated(){
        $this->seed(ProvincesSeeder::class);
        $city = City::inRandomOrder()->first();
        $user = $this->createUser();

        $res = $res = $this->request([
            'PATCH',
            $this->route('update'),[
                'name' => $user->name,
                'city_id' => $city->id
            ]
        ], $user);

        $res->assertStatus(200)->assertExactJson([
            'data' => $user->toArray()
        ]);
    }

    public function test_phone_number_is_valid(){
        $rules = [
            '09141234567' => true,
            '9141234567' => true,
            '989123456789' => true,
            '+989123456789' => true,

            '19141234567' => false,
            '08141234567' => false,
            '91412345672' => false,
        ];
        foreach ($rules as $phone => $ok){
            $res = $res = $this->request([
                'PATCH',
                $this->route('update'),
                compact('phone')
            ]);
            $data = $res->json();

            if($ok){
                $this->assertArrayNotHasKey('phone', $data);
            }else{
                $this->assertArrayHasKey('phone', $data);
            }
        }
    }
    public function test_phone_number_updated(){
        $user = $this->createUser();
        $name = 'new name';
        $phone = '+989123456789';

        $this->request([
            'PATCH',
            $this->route('update'),
            compact('name', 'phone')
        ], $user)->assertOk()->assertExactJson([
            'data' => (new UserResource(
                array_merge($user->toArray(), compact('name', 'phone'))
            ))->resolve()
        ]);
    }

    public function test_national_id_is_valid(){
        $rules = [
            '5235243522' => false,
            '1520493657' => true,
        ];
        foreach ($rules as $national_id => $ok){
            $res = $res = $this->request([
                'PATCH',
                $this->route('update'),
                compact('national_id')
            ]);
            $data = $res->json();

            if($ok){
                $this->assertArrayNotHasKey('national_id', $data);
            }else{
                $this->assertArrayHasKey('national_id', $data);
            }
        }
    }
    public function test_national_id_updated(){
        $user = $this->createUser();
        $name = 'new name';
        $national_id = '1520493657';

        $this->request([
            'PATCH',
            $this->route('update'),
            compact('name', 'national_id')
        ], $user)->assertOk()->assertExactJson([
            'data' => (new UserResource(
                array_merge($user->toArray(), compact('name', 'national_id'))
            ))->resolve()
        ]);
    }
}
