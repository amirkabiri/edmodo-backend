<?php

namespace Tests\Feature;

use App\Models\User;
use Faker\Provider\File;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class UpdatePasswordTest extends TestCase
{
    use RefreshDatabase;

    private $headers = [
        'Accept' => 'application/json'
    ];
    private $name = 'api.v1.profile.';

    private function createUser($data = []){
        return factory(User::class)->create($data);
    }

    public function test_update_password_with_unauthenticate_user(){
        $res = $this->withHeaders($this->headers)->patch(route($this->name . 'update_password'));

        $res->assertStatus(401);
    }
    public function test_update_password_with_invalid_data(){
        $user = $this->createUser();
        $res = $this->actingAs($user, 'api')
            ->withHeaders($this->headers)
            ->patch(route($this->name . 'update_password'));

        $res->assertStatus(422)->assertJsonStructure(['password']);
    }
    public function test_update_password_with_valid_data(){
        $user = $this->createUser();
        $res = $this->actingAs($user, 'api')
            ->withHeaders($this->headers)
            ->patch(route($this->name . 'update_password'), ['password' => 'valid password']);

        $res->assertStatus(204);
    }
    public function test_password_updated_successfully(){
        $password = 'valid password';

        $user = $this->createUser();
        $res = $this->actingAs($user, 'api')
            ->withHeaders($this->headers)
            ->patch(route($this->name . 'update_password'), ['password' => $password]);
        $res->assertStatus(204);

        $this->assertTrue(Hash::check($password, $user->password));
    }
}
