<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\Feature\Profile\Base;

class UpdatePhotoTest extends Base
{
    use RefreshDatabase;

    protected function route($name = 'update_photo', $params = []){
        return route($this->name . $name, $params);
    }

    public function test_upload_photo_with_empty_data(){
        $user = $this->createUser();

        $res = $this->withHeaders($this->headers)
            ->actingAs($user, 'api')
            ->patch($this->route());

        $res->assertStatus(422)->assertJsonStructure(['photo']);
    }
    public function test_upload_photo_is_a_file_or_not(){
        $user = $this->createUser();

        $res = $this->withHeaders($this->headers)
            ->actingAs($user, 'api')
            ->patch($this->route(), [
                'photo' => 'I am not a file, I am string'
            ]);

        $res->assertStatus(422)->assertJsonStructure(['photo']);
    }
    public function test_upload_photo_extension_is_valid(){
        $user = $this->createUser();
        $extensions = [
            'png' => true,
            'jpg' => true,

            'jpeg' => true,
            'gif' => false,
            'svg' => false,
            'webp' => false,
            'bmp' => false,

            'txt' => false,
            'php' => false,
        ];
        foreach ($extensions as $extension => $validation_rule){
            $photo = UploadedFile::fake()->image('file.' . $extension);

            $res = $this->withHeaders($this->headers)
                ->actingAs($user, 'api')
                ->patch($this->route(), [
                    'photo' => $photo
                ]);

            $this->addUserPhotoToAvatars($user);

            $res->assertStatus($validation_rule ? 200 : 422);
            if(!$validation_rule) $res->assertJsonStructure(['photo']);
        }
    }
    public function test_upload_photo_has_valid_size(){
        $max_photo_size = config('config.user_photo_max_size');
        $sizes = [
            $max_photo_size => true,
            $max_photo_size - 100 => true,
            0 => true,
            $max_photo_size + 100 => false
        ];

        foreach ($sizes as $size => $validation_rule){
            $user = $this->createUser();
            $photo = UploadedFile::fake()->image('file.png')->size($size);

            $res = $this->withHeaders($this->headers)
                ->actingAs($user, 'api')
                ->patch($this->route(), [
                    'photo' => $photo
                ]);

            $this->addUserPhotoToAvatars($user);

            $res->assertStatus($validation_rule ? 200 : 422);
        }
    }
    public function test_upload_photo_status_code_with_a_real_photo(){
        $user = $this->createUser();
        $photo = UploadedFile::fake()->image('file.png', 600, 600);

        $res = $this->withHeaders($this->headers)
            ->actingAs($user, 'api')
            ->patch($this->route(), [
                'photo' => $photo
            ]);

        $this->addUserPhotoToAvatars($user);

        $res->assertStatus(200);
    }
    public function test_upload_photo_updates_user_photo_column_in_database(){
        $user = $this->createUser();
        $photo = UploadedFile::fake()->image('file.png', 600, 600);

        $res = $this->withHeaders($this->headers)
            ->actingAs($user, 'api')
            ->patch($this->route(), [
                'photo' => $photo
            ]);

        $this->addUserPhotoToAvatars($user);

        $this->assertEquals($user->photo, $res->content());
    }
    public function test_upload_photo_exists_in_storage(){
        $user = $this->createUser();
        $photo = UploadedFile::fake()->image('file.png', 600, 600);

        $res = $this->withHeaders($this->headers)
            ->actingAs($user, 'api')
            ->patch($this->route(), [
                'photo' => $photo
            ]);

        $this->addUserPhotoToAvatars($user);

        $photo_name = $res->content();
        $this->assertTrue(Storage::exists('avatars/' . $photo_name));
    }
    public function test_former_photo_removed_when_uploading_new_photo(){
        $user = $this->createUser();
        $photo = UploadedFile::fake()->image('file.png', 600, 600);

        $former_response = $this->withHeaders($this->headers)
            ->actingAs($user, 'api')
            ->patch($this->route(), [
                'photo' => $photo
            ]);
        $former_response->assertStatus(200);
        $former_photo = $former_response->content();
        Storage::assertExists('avatars/' . $former_photo);

        $current_response = $this->withHeaders($this->headers)
            ->actingAs($user, 'api')
            ->patch($this->route(), [
                'photo' => $photo
            ]);
        $current_photo = $current_response->content();
        $current_response->assertStatus(200);
        Storage::assertExists('avatars/' . $current_photo);

        Storage::assertMissing('avatars/' . $former_photo);

        $this->addUserPhotoToAvatars($user);
    }

    public function test_photo_thumbnails_exists(){
        $user = $this->createUser();
        $photo = UploadedFile::fake()->image('file.png', 600, 600);

        $response = $this->withHeaders($this->headers)
            ->actingAs($user, 'api')
            ->patch($this->route(), [
                'photo' => $photo
            ]);
        $response->assertStatus(200);
        $photoname = $response->content();

        $thumb_sizes = config('config.user_photo_thumbnails');
        foreach ($thumb_sizes as $thumb_size){
            Storage::assertExists("avatars/{$thumb_size}x{$thumb_size}/" . $photoname);
        }

        $this->addUserPhotoToAvatars($user);
    }

    public function test_former_photo_thumbs_removed_when_uploading_new_photo(){
        $user = $this->createUser();
        $photo = UploadedFile::fake()->image('file.png', 600, 600);

        $former_response = $this->withHeaders($this->headers)
            ->actingAs($user, 'api')
            ->patch($this->route(), [
                'photo' => $photo
            ]);
        $former_photo = $former_response->content();

        $this->withHeaders($this->headers)
            ->actingAs($user, 'api')
            ->patch($this->route(), [
                'photo' => $photo
            ]);

        foreach (config('config.user_photo_thumbnails') as $thumb_size){
            Storage::assertMissing("avatars/{$thumb_size}x{$thumb_size}/" . $former_photo);
        }

        $this->addUserPhotoToAvatars($user);
    }
}
