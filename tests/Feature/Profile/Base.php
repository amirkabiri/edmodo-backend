<?php

namespace Tests\Feature\Profile;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class Base extends TestCase
{
    protected $name = 'api.v1.profile.';
    protected $headers = [
        'Accept' => 'application/json'
    ];
    protected $avatars = [];

    protected function addUserPhotoToAvatars(User $user){
        $user = $user->refresh();
        $this->avatars[] = $user->photo;
    }

    protected function route($name, $params = []){
        return route($this->name . $name, $params);
    }
    protected function request($arg1, $user = null){
        if(count($arg1) === 2) $arg1[] = [];
        list($method, $route, $params) = $arg1;

        if($user === false){
            return $this->withHeaders($this->headers)->json(
                $method,
                $route,
                $params
            );
        }

        if($user === null) $user = factory(User::class)->create();

        $response = $this->actingAs($user, 'api')->withHeaders($this->headers)->json(
            $method,
            $route,
            $params
        );

        return $response;
    }

    protected function createUser($data = []){
        return factory(User::class)->create($data);
    }

    protected function tearDown(): void
    {
        foreach ($this->avatars as $avatar){
            if(Storage::exists("avatars/$avatar")) {
                Storage::delete("avatars/$avatar");
            }

            foreach (config('config.user_photo_thumbnails') as $size){
                if(Storage::exists("avatars/{$size}x{$size}/$avatar")) {
                    Storage::delete("avatars/{$size}x{$size}/$avatar");
                }
            }
        }

        parent::tearDown();
    }
}