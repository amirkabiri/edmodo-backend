<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\Feature\Profile\Base;
use Tests\TestCase;

class ShowPhotoTest extends Base
{
    use RefreshDatabase;

    protected function route($name = 'show_photo', $params = []){
        return route($this->name . $name, $params);
    }

    public function test_status_user_has_not_photo(){
        $user = $this->createUser();
        $response = $this->withHeaders($this->headers)
            ->actingAs($user, 'api')
            ->get($this->route());

        $response->assertStatus(404);
    }
    public function test_get_real_size_photo(){
        $user = $this->createUser();
        $photo = UploadedFile::fake()->image('file.jpg');

        $response = $this->withHeaders($this->headers)->actingAs($user, 'api')
            ->patch($this->route('update_photo'), ['photo' => $photo]);

        $this->addUserPhotoToAvatars($user);

        $response = $this->withHeaders($this->headers)
            ->actingAs($user, 'api')
            ->get($this->route());

        $response->assertStatus(200);
    }
    public function test_get_photo_thumbs_status(){
        $user = $this->createUser();
        $photo = UploadedFile::fake()->image('file.jpg');

        $this->withHeaders($this->headers)->actingAs($user, 'api')
            ->patch($this->route('update_photo'), ['photo' => $photo]);

        $this->addUserPhotoToAvatars($user);

        foreach (config('config.user_photo_thumbnails') as $size){
            $response = $this->withHeaders($this->headers)
                ->actingAs($user, 'api')
                ->get($this->route('show_photo', ['size' => $size]));

            $response->assertStatus(200);
        }
    }
}