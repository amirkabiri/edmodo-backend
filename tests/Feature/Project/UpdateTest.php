<?php

namespace Tests\Feature\Project;

use App\Models\Course;
use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Resources\Project as ProjectResource;

class UpdateTest extends Base
{
    use RefreshDatabase;

    public function test_status_with_invalid_user(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());

        $response = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'project' => $project->id]),
        ]);

        $response->assertForbidden();
    }
    public function test_status_with_member(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());
        $member = $course->members()->save(factory(User::class)->make());

        $response = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'project' => $project->id]),
        ], $member);

        $response->assertForbidden();
    }
    public function test_status_with_teacher(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());

        $response = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'project' => $project->id]),
        ], $teacher);

        $response->assertStatus(422);
    }
    public function test_validation(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());

        $response = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'project' => $project->id]),
        ], $teacher);

        $response->assertStatus(422)->assertJsonStructure(['title']);
    }
    public function test_invalid_project_id(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'project' => 'invalid project id']),
        ], $teacher);

        $response->assertNotFound();
    }
    public function test_status_success(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());

        $title = 'new title';
        $description = 'new description';
        $deadline = now()->timestamp;

        $response = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'project' => $project->id]),
            compact('title', 'description', 'deadline')
        ], $teacher);

        $response->assertOk();
    }
    public function test_project_updated_in_db(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());

        $title = 'new title';
        $description = 'new description';
        $deadline = now()->timestamp;

        $response = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'project' => $project->id]),
            compact('title', 'description', 'deadline')
        ], $teacher);

        $project = $project->refresh();

        $this->assertEquals($title, $project->title);
        $this->assertEquals($description, $project->description);
        $this->assertEquals($deadline, $project->deadline->timestamp);
    }
    public function test_success_structure(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());

        $title = 'new title';
        $description = 'new description';
        $deadline = now();

        $response = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'project' => $project->id]),
            ['title' => $title, 'description' => $description, 'deadline' => $deadline->timestamp]
        ], $teacher);

        $response->assertExactJson([
            'data' => (new ProjectResource(
                array_merge(
                    $project->toArray(),
                    ['title' => $title, 'description' => $description, 'deadline' => $deadline->toDateTimeString()])
                )
            )->resolve()
        ]);
    }

}
