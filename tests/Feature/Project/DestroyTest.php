<?php

namespace Tests\Feature\Project;

use App\Models\Course;
use App\Models\Project;
use App\Models\Solution;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class DestroyTest extends Base
{
    use RefreshDatabase;

    public function test_status_with_invalid_user(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());

        $response = $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'project' => $project->id])
        ]);

        $response->assertForbidden();
    }
    public function test_status_with_member(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());
        $member = $course->members()->save(factory(User::class)->make());

        $response = $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'project' => $project->id])
        ], $member);

        $response->assertForbidden();
    }
    public function test_status_with_teacher(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());

        $response = $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'project' => $project->id])
        ], $teacher);

        $response->assertNoContent();
    }
    public function test_project_deleted_from_db(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());

        $response = $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'project' => $project->id])
        ], $teacher);

        $this->assertNull(Project::find($project->id));
    }
    public function test_with_invalid_project_id(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'project' => 'invalid project id'])
        ], $teacher);

        $response->assertNotFound();
    }

    public function test_solution_files_of_project_removed(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $member = $course->members()->save(factory(User::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());
        $file = UploadedFile::fake()->create('file.zip');

        $res = $this->request([
            'POST',
            $this->route('solutions.store', ['course' => $course->id, 'project' => $project->id]),
            compact('file')
        ], $member);
        $res->assertCreated();
        $solution = Solution::find($res->json()['data']['id']);
        Storage::assertExists('solutions/' . $solution->filepath);

        $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'project' => $project->id])
        ], $teacher)->assertNoContent();

        Storage::assertMissing('solutions/' . $solution->filepath);
    }
}
