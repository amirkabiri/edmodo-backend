<?php

namespace Tests\Feature\Project;

use App\Http\Resources\Project as ProjectResource;
use App\Models\Course;
use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StoreTest extends Base
{
    use RefreshDatabase;

    public function test_status_with_invalid_user(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->request([
            'POST',
            $this->route('store', ['course' => $course->id])
        ]);

        $response->assertForbidden();
    }
    public function test_status_with_member(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $member = $course->members()->save(factory(User::class)->make());

        $response = $this->request([
            'POST',
            $this->route('store', ['course' => $course->id])
        ], $member);

        $response->assertForbidden();
    }
    public function test_status_with_teacher(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->request([
            'POST',
            $this->route('store', ['course' => $course->id])
        ], $teacher);

        $response->assertStatus(422);
    }
    public function test_validation(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->request([
            'POST',
            $this->route('store', ['course' => $course->id])
        ], $teacher);

        $response->assertStatus(422)->assertJsonStructure(['title']);
    }
    public function test_project_inserted_in_db(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());

        $title = 'hello this is my title';
        $description = 'hello this is my description';

        $response = $this->request([
            'POST',
            $this->route('store', ['course' => $course->id]),
            compact('title', 'description')
        ], $teacher);

        $this->assertNotNull(Project::where('title', $title)->where('description', $description)->first());
    }
    public function test_success_structure(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());

        $title = 'hello this is my title';
        $description = 'hello this is my description';

        $response = $this->request([
            'POST',
            $this->route('store', ['course' => $course->id]),
            compact('title', 'description')
        ], $teacher);

        $project = Project::whereTitle($title)->whereDescription($description)->first();
        $data = new ProjectResource(array_merge($project->toArray(), compact('title', 'description')));

        $response->assertExactJson([
            'data' => $data->resolve()
        ]);
    }
}
