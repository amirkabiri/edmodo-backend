<?php

namespace Tests\Feature\Project;

use App\Models\Course;
use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Resources\Project as ProjectResource;

class ShowTest extends Base
{
    use RefreshDatabase;

    public function test_status_with_invalid_user(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());

        $response = $this->request([
            'GET',
            $this->route('show', ['course' => $course->id, 'project' => $project->id])
        ]);

        $response->assertForbidden();
    }
    public function test_status_with_member(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());
        $member = $course->members()->save(factory(User::class)->make());

        $response = $this->request([
            'GET',
            $this->route('show', ['course' => $course->id, 'project' => $project->id])
        ], $member);

        $response->assertOk();
    }
    public function test_status_with_teacher(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());

        $response = $this->request([
            'GET',
            $this->route('show', ['course' => $course->id, 'project' => $project->id])
        ], $teacher);

        $response->assertOk();
    }
    public function test_structure_success(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());

        $response = $this->request([
            'GET',
            $this->route('show', ['course' => $course->id, 'project' => $project->id])
        ], $teacher);

        $response->assertExactJson([
            'data' => (new ProjectResource($project))->resolve()
        ]);
    }
}
