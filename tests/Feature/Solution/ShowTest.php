<?php

namespace Tests\Feature\Solution;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Resources\Solution as SolutionResource;

class ShowTest extends Base
{
    use RefreshDatabase;

    public function test_user_is_not_joined_in_course_can_see_solution(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);

        $res = $this->request([
            'GET',
            $this->route('show', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id])
        ]);

        $res->assertForbidden();
    }
    public function test_teacher_can_see_solution(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);

        $res = $this->request([
            'GET',
            $this->route('show', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id])
        ], $teacher);

        $res->assertOk();
    }
    public function test_member_can_see_other_member_solution(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);
        $otherMember = $this->makeMember($course);

        $res = $this->request([
            'GET',
            $this->route('show', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id])
        ], $otherMember);

        $res->assertForbidden();
    }
    public function test_member_can_see_self_solution(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);

        $res = $this->request([
            'GET',
            $this->route('show', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id])
        ], $member);

        $res->assertOk();
    }
    public function test_structure(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);

        $res = $this->request([
            'GET',
            $this->route('show', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id])
        ], $member);

        $res->assertExactJson([
            'data' => (new SolutionResource($solution))->resolve()
        ]);
    }
}
