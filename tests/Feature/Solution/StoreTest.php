<?php

namespace Tests\Feature\Solution;

use App\Models\Solution;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use App\Http\Resources\Solution as SolutionResource;
use Illuminate\Support\Facades\Storage;

class StoreTest extends Base
{
    use RefreshDatabase;

    public function test_user_is_not_joined_in_course_can_store_solution(){
        list($teacher, $course, $project) = $this->makeProject();

        $res = $this->request([
            'POST',
            $this->route('store', ['course' => $course->id, 'project' => $project])
        ]);

        $res->assertForbidden();
    }
    public function test_status_member_can_store_solution(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);

        $res = $this->request([
            'POST',
            $this->route('store', ['course' => $course->id, 'project' => $project->id])
        ], $member);

        $res->assertStatus(422);
    }
    public function test_file_is_required(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);

        $res = $this->request([
            'POST',
            $this->route('store', ['course' => $course->id, 'project' => $project->id]),
            ['file' => 'text']
        ], $member);

        $res->assertStatus(422);
    }
    public function test_file_should_be_zip_or_rar(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);

        $rules = [
            'zip' => true, 'rar' => true, 'php' => false, 'py' => false, 'jpg' => false,
            'png' => false, 'txt' => false, 'html' => false
        ];

        foreach ($rules as $type => $ok){
            $file = UploadedFile::fake()->create('file.' . $type);

            $res = $this->request([
                'POST',
                $this->route('store', ['course' => $course->id, 'project' => $project->id]),
                compact('file')
            ], $member);

            if($res->getStatusCode() == 201){
                $solution_id = $res->json()['data']['id'];
                $solution = Solution::find($solution_id);
                $this->addSolutionPhotoToDeleteQueue($solution);
            }

            $res->assertStatus($ok ? 201 : 422);
        }
    }
    public function test_solution_successful_creation(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);

        $file = UploadedFile::fake()->create('file.zip');

        $res = $this->request([
            'POST',
            $this->route('store', ['course' => $course->id, 'project' => $project->id]),
            compact('file')
        ], $member);

        $solution = Solution::find($res->json()['data']['id']);
        $this->addSolutionPhotoToDeleteQueue($solution);

        $res->assertCreated()->assertExactJson([
            'data' => (new SolutionResource($project->solutions()->first()))->resolve()
        ]);
    }
    public function test_solution_saved_in_db(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);

        $file = UploadedFile::fake()->create('file.zip');

        $res = $this->request([
            'POST',
            $this->route('store', ['course' => $course->id, 'project' => $project->id]),
            compact('file')
        ], $member);

        $solution = Solution::find($res->json()['data']['id']);
        $this->addSolutionPhotoToDeleteQueue($solution);

        $this->assertEquals(1, $member->solutions()->count());
    }
    public function test_filename_saved_correctly_in_db(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);

        $filename = 'my-filename.zip';
        $file = UploadedFile::fake()->create($filename);

        $res = $this->request([
            'POST',
            $this->route('store', ['course' => $course->id, 'project' => $project->id]),
            compact('file')
        ], $member);

        $solution_id = $res->json()['data']['id'];
        $solution = Solution::find($solution_id);

        $this->addSolutionPhotoToDeleteQueue($solution);

        $this->assertEquals($solution->filename, $filename);
    }
    public function test_file_saved_in_storage(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);

        $filename = 'my-filename.zip';
        $file = UploadedFile::fake()->create($filename);

        $res = $this->request([
            'POST',
            $this->route('store', ['course' => $course->id, 'project' => $project->id]),
            compact('file')
        ], $member);

        $solution_id = $res->json()['data']['id'];
        $solution = Solution::find($solution_id);

        $this->addSolutionPhotoToDeleteQueue($solution);

        Storage::assertExists('solutions/' . $solution->filepath);
    }
}
