<?php

namespace Tests\Feature\Solution;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Resources\Solution as SolutionResource;

class IndexTest extends Base
{
    use RefreshDatabase;

    public function test_status_with_invalid_user(){
        list($teacher, $course, $project) = $this->makeProject();

        $res = $this->request([
            'GET',
            $this->route('index', ['course' => $course->id, 'project' => $project->id])
        ]);

        $res->assertForbidden();
    }
    public function test_status_with_member(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);

        $res = $this->request([
            'GET',
            $this->route('index', ['course' => $course->id, 'project' => $project->id])
        ], $member);

        $res->assertOk();
    }
    public function test_status_with_teacher(){
        list($teacher, $course, $project) = $this->makeProject();

        $res = $this->request([
            'GET',
            $this->route('index', ['course' => $course->id, 'project' => $project->id])
        ], $teacher);

        $res->assertOk();
    }
    public function test_teacher_can_see_members_solutions(){
        list($teacher, $course, $project) = $this->makeProject();

        $member = $this->makeMember($course);
        $this->joinMemberInCourse($course, $member);
        $solution = $this->makeSolution($project, $member);

        $res = $this->request([
            'GET',
            $this->route('index', ['course' => $course->id, 'project' => $project->id])
        ], $teacher);

        $res->assertOk()->assertExactJson([
            'data' => SolutionResource::collection([$solution])->resolve()
        ]);
    }
    public function test_member_can_see_self_solutions(){
        list($teacher, $course, $project) = $this->makeProject();

        $member = $this->makeMember($course);
        $this->joinMemberInCourse($course, $member);
        $solution = $this->makeSolution($project, $member);

        $res = $this->request([
            'GET',
            $this->route('index', ['course' => $course->id, 'project' => $project->id])
        ], $member);

        $res->assertOk()->assertExactJson([
            'data' => SolutionResource::collection([$solution])->resolve()
        ]);
    }
    public function test_member_can_see_other_members_solutions(){
        list($teacher, $course, $project) = $this->makeProject();

        $member = $this->makeMember($course);

        $otherMember = $this->makeMember($course);
        $this->joinMemberInCourse($course, $otherMember);
        $solution = $this->makeSolution($project, $otherMember);

        $res = $this->request([
            'GET',
            $this->route('index', ['course' => $course->id, 'project' => $project->id])
        ], $member);

        $res->assertOk()->assertExactJson([
            'data' => SolutionResource::collection([])->resolve()
        ]);
    }
}
