<?php

namespace Tests\Feature\Solution;

use App\Models\Solution;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class DestroyTest extends Base
{
    use RefreshDatabase;

    public function test_user_is_not_joined_in_course_can_delete_solution(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);

        $res = $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id])
        ]);

        $res->assertForbidden();
    }
    public function test_teacher_can_delete_solution(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);

        $res = $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id])
        ], $teacher);

        $res->assertForbidden();
    }
    public function test_member_can_delete_other_member_solution(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);
        $otherMember = $this->makeMember($course);

        $res = $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id])
        ], $otherMember);

        $res->assertForbidden();
    }
    public function test_member_can_delete_self_solution(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);

        $res = $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id])
        ], $member);

        $res->assertNoContent();
    }
    public function test_solution_remove_from_db(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);

        $res = $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id])
        ], $member);

        $this->assertNull(Solution::find($solution->id));
    }
    public function test_solution_file_removed(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);

        $storeFile = UploadedFile::fake()->create('file.zip');
        $storeRes = $this->request([
            'POST',
            $this->route('store', ['course' => $course->id, 'project' => $project->id]),
            ['file' => $storeFile]
        ], $member);
        $solution = Solution::find($storeRes->json()['data']['id']);
        $this->addSolutionPhotoToDeleteQueue($solution);

        Storage::assertExists('solutions/' . $solution->filepath);

        $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id])
        ], $member);

        Storage::assertMissing('solutions/' . $solution->filepath);
    }
}
