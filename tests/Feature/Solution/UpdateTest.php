<?php

namespace Tests\Feature\Solution;

use App\Models\Solution;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use App\Http\Resources\Solution as SolutionResource;
use Illuminate\Support\Facades\Storage;

class UpdateTest extends Base
{
    use RefreshDatabase;

    public function test_user_is_not_joined_in_course_can_update_solution(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);

        $res = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id])
        ]);

        $res->assertForbidden();
    }
    public function test_teacher_can_update_solution(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);

        $res = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id])
        ], $teacher);

        $res->assertForbidden();
    }
    public function test_member_can_update_other_member_solution(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);
        $otherMember = $this->makeMember($course);

        $res = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id])
        ], $otherMember);

        $res->assertForbidden();
    }
    public function test_member_can_update_self_solution(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);

        $res = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id])
        ], $member);

        $res->assertStatus(422);
    }
    public function test_file_is_required(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);

        $res = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id]),
            ['file' => 'text']
        ], $member);

        $res->assertStatus(422)->assertJsonStructure(['file']);
    }
    public function test_file_should_be_zip_or_rar(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);

        $rules = [
            'zip' => true, 'rar' => true, 'php' => false, 'py' => false, 'jpg' => false,
            'png' => false, 'txt' => false, 'html' => false
        ];

        foreach ($rules as $type => $ok){
            $file = UploadedFile::fake()->create('file.' . $type);

            $res = $this->request([
                'PATCH',
                $this->route('update', ['course' => $course->id, 'project' => $project->id, 'solution' => $solution->id]),
                compact('file')
            ], $member);

            $this->addSolutionPhotoToDeleteQueue($solution);

            $res->assertStatus($ok ? 200 : 422);
            if(!$ok) $res->assertJsonStructure(['file']);
        }
    }
    public function test_update_solution_is_successful(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);

        $filename = 'new-file.zip';
        $file = UploadedFile::fake()->create($filename);

        $res = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id]),
            compact('file')
        ], $member);

        $this->addSolutionPhotoToDeleteQueue($solution);

        $res->assertOk()->assertExactJson([
            'data' => (new SolutionResource(
                array_merge($solution->toArray(), compact('filename'))
            ))->resolve()
        ]);
    }
    public function test_filename_correctly_updated_in_db(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);

        $filename = 'new-file.zip';
        $file = UploadedFile::fake()->create($filename);

        $res = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id]),
            compact('file')
        ], $member);

        $this->addSolutionPhotoToDeleteQueue($solution);

        $solution = Solution::find($res->json()['data']['id']);
        $this->assertEquals($filename, $solution->filename);
    }
    public function test_file_saved_in_storage(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);
        $solution = $this->makeSolution($project, $member);

        $filename = 'new-file.zip';
        $file = UploadedFile::fake()->create($filename);

        $res = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id]),
            compact('file')
        ], $member);

        $this->addSolutionPhotoToDeleteQueue($solution);

        $solution = Solution::find($res->json()['data']['id']);
        Storage::assertExists('solutions/' . $solution->filepath);
    }
    public function test_previous_file_removed_from_storage(){
        list($teacher, $course, $project) = $this->makeProject();
        $member = $this->makeMember($course);

        $storeFile = UploadedFile::fake()->create('file.zip');
        $storeRes = $this->request([
            'POST',
            $this->route('store', ['course' => $course->id, 'project' => $project->id]),
            ['file' => $storeFile]
        ], $member);
        $solution = Solution::find($storeRes->json()['data']['id']);

        Storage::assertExists('solutions/' . $solution->filepath);

        $updateFile = UploadedFile::fake()->create('new-file.zip');
        $updateRes = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'project' => $project, 'solution' => $solution->id]),
            ['file' => $updateFile]
        ], $member);

        Storage::assertMissing('solutions/' . $solution->filepath);

        $this->addSolutionPhotoToDeleteQueue($solution);
    }
}
