<?php

namespace Tests\Feature\Solution;
use App\Models\Course;
use App\Models\Project;
use App\Models\Solution;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class Base extends TestCase
{
    protected $name = 'api.v1.courses.projects.solutions.';
    protected $headers = [
        'Accept' => 'application/json'
    ];
    protected $solutionPhotos = [];
    protected function addSolutionPhotoToDeleteQueue(Solution $solution){
        $solution = $solution->refresh();
        $this->solutionPhotos[] = $solution->filepath;
    }

    protected function tearDown(): void
    {
        foreach ($this->solutionPhotos as $solutionPhoto){
            if(Storage::exists("solutions/$solutionPhoto")) {
                Storage::delete("solutions/$solutionPhoto");
            }
        }

        parent::tearDown();
    }

    protected function route($name, $params = []){
        return route($this->name . $name, $params);
    }
    protected function request($arg1, $user = null){
        if(count($arg1) === 2) $arg1[] = [];
        list($method, $route, $params) = $arg1;

        if($user === null) $user = factory(User::class)->create();

        $response = $this->actingAs($user, 'api')->withHeaders($this->headers)->json(
            $method,
            $route,
            $params
        );

        return $response;
    }
    protected function makeProject(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $project = $course->projects()->save(factory(Project::class)->make());

        return [$teacher, $course, $project];
    }
    protected function makeMember(Course $course) : User{
        return $course->members()->save(factory(User::class)->make());
    }
    protected function joinMemberInCourse(Course $course, User $user){
        $course->members()->attach($user->id);
    }
    protected function makeSolution(Project $project, User $user) : Solution{
        return $project->solutions()->save(factory(Solution::class)->make([
            'user_id' => $user->id
        ]));
    }
}