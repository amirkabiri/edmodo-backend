<?php

namespace Tests\Feature\Event;

use App\Models\Course;
use App\Models\Event;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Http\Resources\Event as EventResource;

class StoreTest extends Base
{
    use RefreshDatabase;

    public function test_status_with_a_member(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $member = $course->members()->save(factory(User::class)->make());

        $response = $this->request([
            'POST', $this->route('store', ['course' => $course->id])
        ], $member);

        $response->assertStatus(403);
    }
    public function test_status_with_teacher(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->request([
            'POST', $this->route('store', ['course' => $course->id])
        ], $teacher);

        $response->assertStatus(422);
    }

    public function test_title_is_required(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->request([
            'POST', $this->route('store', ['course' => $course->id])
        ], $teacher);

        $response->assertStatus(422)->assertJsonStructure(['title']);
    }

    public function test_status_with_valid_data(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $title = 'valid title';

        $response = $this->request([
            'POST', $this->route('store', ['course' => $course->id]),['title' => $title]
        ], $teacher);

        $response->assertStatus(201);
    }
    public function test_structure_with_valid_data(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $title = 'valid title';

        $response = $this->request([
            'POST', $this->route('store', ['course' => $course->id]),['title' => $title]
        ], $teacher);
        $id = $response->json()['data']['id'];

        $event = new EventResource(Event::find($id));

        $response->assertExactJson([
            'data' => $event->resolve()
        ]);
    }
    public function test_event_created_in_database(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $title = 'valid title';

        $response = $this->request([
            'POST', $this->route('store', ['course' => $course->id]),['title' => $title]
        ], $teacher);
        $data = $response->json()['data'];

        $this->assertNotNull(Event::find($data['id']));
    }

}
