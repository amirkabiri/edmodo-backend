<?php

namespace Tests\Feature\Event;

use App\Models\Course;
use App\Models\Event;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class DestroyTest extends Base
{
    use RefreshDatabase;

    public function test_status_with_invalid_user(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $event = $course->events()->save(factory(Event::class)->make());

        $response = $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'event' => $event->id])
        ]);

        $response->assertStatus(403);
    }
    public function test_status_with_member(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $member = $course->members()->save(factory(User::class)->make());
        $event = $course->events()->save(factory(Event::class)->make());

        $response = $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'event' => $event->id])
        ], $member);

        $response->assertStatus(403);
    }
    public function test_status_with_teacher(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $event = $course->events()->save(factory(Event::class)->make());

        $response = $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'event' => $event->id])
        ], $teacher);

        $response->assertStatus(204);
    }
    public function test_event_removed_from_db(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $event = $course->events()->save(factory(Event::class)->make());

        $response = $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'event' => $event->id])
        ], $teacher);

        $this->assertNull(Event::find($event->id));
    }

    public function test_event_removed(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $event = $course->events()->save(factory(Event::class)->make());

        $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'event' => $event->id])
        ], $teacher);
        $response = $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'event' => $event->id])
        ], $teacher);

        $response->assertStatus(404);
    }
    public function test_with_invalid_event_id(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->request([
            'DELETE',
            $this->route('destroy', ['course' => $course->id, 'event' => 24234])
        ], $teacher);

        $response->assertStatus(404);
    }


}
