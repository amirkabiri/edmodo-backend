<?php

namespace Tests\Feature\Event;

use App\Models\Course;
use App\Models\Event;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Resources\Event as EventResource;

class IndexTest extends Base
{
    use RefreshDatabase;

    public function test_status_invalid_user_can_see_events(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->request([
            'GET',
            $this->route('index', ['course' => $course->id])
        ]);

        $response->assertStatus(403);
    }
    public function test_status_teacher_can_see_events(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->request([
            'GET',
            $this->route('index', ['course' => $course->id])
        ], $teacher);

        $response->assertStatus(200);
    }
    public function test_status_student_can_see_events(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $student = $course->members()->save(factory(User::class)->make());

        $response = $this->request([
            'GET',
            $this->route('index', ['course' => $course->id])
        ], $student);

        $response->assertStatus(200);
    }
    public function test_structure(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $course->events()->save(factory(Event::class)->make());

        $response = $this->request([
            'GET',
            $this->route('index', ['course' => $course->id])
        ], $teacher);

        $response->assertExactJson([
            'data' => EventResource::collection($course->events)->resolve()
        ]);
    }
    public function test_output_count(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $course->events()->save(factory(Event::class)->make());
        $course->events()->save(factory(Event::class)->make());

        $response = $this->request([
            'GET',
            $this->route('index', ['course' => $course->id])
        ], $teacher);

        $response->assertJsonCount(2, 'data');
    }
}
