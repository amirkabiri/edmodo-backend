<?php

namespace Tests\Feature\Event;

use App\Models\Course;
use App\Models\Event;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Resources\Event as EventResource;

class UpdateTest extends Base
{
    use RefreshDatabase;

    public function test_status_with_invalid_event_id(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());

        $response = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'event' => 324])
        ]);

        $response->assertStatus(404);
    }
    public function test_status_with_invalid_user(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $event = $course->events()->save(factory(Event::class)->make());

        $response = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'event' => $event->id])
        ]);

        $response->assertStatus(403);
    }
    public function test_status_with_member(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $member = $course->members()->save(factory(User::class)->make());
        $event = $course->events()->save(factory(Event::class)->make());

        $response = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'event' => $event->id])
        ], $member);

        $response->assertStatus(403);
    }
    public function test_status_with_teacher(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $event = $course->events()->save(factory(Event::class)->make());

        $response = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'event' => $event->id])
        ], $teacher);

        $response->assertStatus(422);
    }
    public function test_title_is_required(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $event = $course->events()->save(factory(Event::class)->make());

        $response = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'event' => $event->id]),
        ], $teacher);

        $response->assertStatus(422)->assertJsonStructure(['title']);
    }
    public function test_status_with_valid_data(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $event = $course->events()->save(factory(Event::class)->make());

        $title = 'new title';
        $description = 'new description';

        $response = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'event' => $event->id]),
            compact('title', 'description')
        ], $teacher);

        $response->assertStatus(200);
    }
    public function test_update_will_apply_in_database(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $event = $course->events()->save(factory(Event::class)->make());

        $title = 'new title';
        $description = 'new description';

        $response = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'event' => $event->id]),
            compact('title', 'description')
        ], $teacher);

        $event = $event->refresh();
        $this->assertEquals($title, $event->title);
        $this->assertEquals($description, $event->description);
    }
    public function test_structure_with_valid_data(){
        $teacher = factory(User::class)->create();
        $course = $teacher->coursesAsTeacher()->save(factory(Course::class)->make());
        $event = $course->events()->save(factory(Event::class)->make());

        $title = 'new title';
        $description = 'new description';

        $response = $this->request([
            'PATCH',
            $this->route('update', ['course' => $course->id, 'event' => $event->id]),
            compact('title', 'description')
        ], $teacher);

        $data = new EventResource(array_merge($event->toArray(), compact('title', 'description')));

        $response->assertExactJson([
            'data' => $data->resolve()
        ]);
    }
}
