<?php

namespace Tests\Feature\Event;
use App\Models\User;
use Tests\TestCase;

class Base extends TestCase
{
    protected $name = 'api.v1.courses.events.';
    protected $headers = [
        'Accept' => 'application/json'
    ];

    protected function route($name, $params = []){
        return route($this->name . $name, $params);
    }
    protected function request($arg1, $user = null){
        if(count($arg1) === 2) $arg1[] = [];
        list($method, $route, $params) = $arg1;

        if($user === null) $user = factory(User::class)->create();

        $response = $this->actingAs($user, 'api')->withHeaders($this->headers)->json(
            $method,
            $route,
            $params
        );

        return $response;
    }
}