<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProvinceControllerTest extends TestCase
{
    use RefreshDatabase;

    private $name = 'api.v1.provinces';
    private $headers = [
        'Accept' => 'application/json'
    ];

    public function test_status_code_is_200(){
        $user = factory(User::class)->create();
        $res = $this->actingAs($user)
            ->withHeaders($this->headers)
            ->get(route($this->name));

        $res->assertStatus(200);
    }

    public function test_structure_is_valid(){
        $user = factory(User::class)->create();
        $res = $this->actingAs($user)
            ->withHeaders($this->headers)
            ->get(route($this->name));

        $res->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'cities' => [
                        '*' => [
                            'id',
                            'name'
                        ]
                    ]
                ]
            ]
        ]);
    }
}
