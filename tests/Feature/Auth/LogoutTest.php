<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class LogoutTest extends TestCase
{
    use RefreshDatabase;

    private $name = 'api.v1.logout';
    private $headers = [
        'Accept' => 'application/json'
    ];

    public function test_must_logged_in_site(){
        $response = $this->withHeaders($this->headers)->post(route($this->name));

        $response->assertStatus(401);
    }


    // FIXME test_logout_action not passes. fix me please
    public function test_logout_action(){
        $user = factory(User::class)->create([
            'password' => Hash::make('1234')
        ]);
        $response = $this->withHeaders($this->headers)->post(route('api.v1.login'), [
            'email' => $user->email,
            'password' => '1234'
        ]);


        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $response->content(),
            'Accept' => 'application/json'
        ])->post(route($this->name));
        $response->assertStatus(204);

        $this->assertEquals(null, User::find($user->id)->api_token);

//        $response = $this->withHeaders([
//            'Accept' => 'application/json'
//        ])->post(route($this->name));
//        return dd($response->status());
//        $response->assertStatus(401);
    }
}
