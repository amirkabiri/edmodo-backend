<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    private $name = 'api.v1.login';

    private function route(){
        return route($this->name);
    }

    public function test_without_data()
    {
        $response = $this->post($this->route());

        $response->assertStatus(422);
    }

    public function test_with_invalid_email(){
        $response = $this->post($this->route(), [
            'email' => 'hello',
            'password' => 'hello world'
        ]);

        $response->assertStatus(422)->assertJsonStructure(['email']);
    }

    public function test_with_email_not_exists(){
        $response = $this->post($this->route(), [
            'email' => 'hellofsdfs@email_that_not_exists',
            'password' => 'fdsfs'
        ]);

        $response->assertStatus(422)->assertJsonStructure(['email']);
    }

    public function test_with_wrong_password(){
        $user = factory(User::class)->create();

        $response = $this->post($this->route(), [
            'email' => $user->email,
            'password' => 'this is a wrong password'
        ]);

        $response->assertStatus(422)->assertJsonStructure(['password']);
    }

    public function test_with_valid_data(){
        $user = factory(User::class)->create([
            'password' => Hash::make('true password')
        ]);

        $response = $this->post($this->route(), [
            'email' => $user->email,
            'password' => 'true password'
        ]);

        $response->assertStatus(200);
    }

    public function test_login_returns_token(){
        $user = factory(User::class)->create([
            'password' => Hash::make('true password')
        ]);

        $response = $this->post($this->route(), [
            'email' => $user->email,
            'password' => 'true password'
        ]);

        $this->assertTrue(strlen($response->content()) > 0);
    }

    public function test_token_exists_in_database(){
        $user = factory(User::class)->create([
            'password' => Hash::make('true password')
        ]);

        $response = $this->post($this->route(), [
            'email' => $user->email,
            'password' => 'true password'
        ]);

        $search = User::where('api_token', $response->content())->count();
        $this->assertEquals($search, 1);
    }

    public function test_token_is_for_the_true_user(){
        $user = factory(User::class)->create([
            'password' => Hash::make('true password')
        ]);

        $response = $this->post($this->route(), [
            'email' => $user->email,
            'password' => 'true password'
        ]);

        $search = User::where('api_token', $response->content())->first();
        $this->assertEquals($user->email, $search->email);
    }
}
