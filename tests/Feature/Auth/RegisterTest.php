<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    private $name = 'api.v1.register';

    private function route(){
        return route($this->name);
    }

    public function test_without_data(){
        $res = $this->post($this->route());

        $res->assertStatus(422);
    }

    public function test_with_invalid_email(){
        $res = $this->post($this->route(), [
            'email' => 'invalid email'
        ]);

        $res->assertStatus(422)->assertJsonStructure(['email', 'password', 'name']);
    }

    public function test_with_valid_email(){
        $res = $this->post($this->route(), [
            'email' => 'invaliddemail@dsafs'
        ]);

        $res->assertStatus(422)->assertJsonStructure(['password', 'name']);
    }

    public function test_with_email_which_already_exists(){
        $user = factory(User::class)->create();

        $res = $this->post($this->route(), [
            'email' => $user->email,
            'password' => 'this is valid password',
            'name' => 'this is valid name'
        ]);

        $res->assertStatus(422)->assertJsonStructure(['email']);
    }

    public function test_with_password(){
        $res = $this->post($this->route(), [
            'password' => 'this is valid password'
        ]);

        $res->assertStatus(422)->assertJsonStructure(['email', 'name']);
    }

    public function test_with_name(){
        $res = $this->post($this->route(), [
            'name' => 'this is valid name'
        ]);

        $res->assertStatus(422)->assertJsonStructure(['email', 'password']);
    }

    public function test_with_valid_data(){
        $res = $this->post($this->route(), [
            'name' => 'this is valid name',
            'password' => 'this is valid password',
            'email' => 'valid@email.com'
        ]);

        $res->assertStatus(201);
    }

    public function test_register_returns_token(){
        $res = $this->post($this->route(), [
            'name' => 'this is valid name',
            'password' => 'this is valid password',
            'email' => 'valid@email.com'
        ]);

        $this->assertTrue(strlen($res->content()) > 0);
    }

    public function test_token_exists_in_database(){
        $res = $this->post($this->route(), [
            'name' => 'this is valid name',
            'password' => 'this is valid password',
            'email' => 'valid@email.com'
        ]);
        $token = $res->content();

        $search = User::where('api_token', $token)->count();

        $this->assertEquals(1, $search);
    }

    public function test_token_is_for_the_true_user(){
        $user = factory(User::class)->make();
        $res = $this->post($this->route(), [
            'name' => $user->name,
            'password' => 'this is valid password',
            'email' => $user->email
        ]);
        $token = $res->content();

        $search = User::where('api_token', $token)->first();

        $this->assertEquals($user->email, $search->email);
    }
}
