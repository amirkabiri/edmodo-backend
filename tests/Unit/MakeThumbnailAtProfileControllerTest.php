<?php

namespace Tests\Unit;

use App\Http\Controllers\V1\ProfileController;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class MakeThumbnailAtProfileControllerTest extends TestCase
{
    private $avatars = [];

    public function test_makeThumbnails_method_exists_in_ProfileController(){
        $profileController = new ProfileController();

        $this->assertTrue(method_exists($profileController, 'makeThumbnails'));
    }
    public function test_thumbnails_exists(){
        $name = uniqid() . '.png';
        $this->avatars[] = $name;
        $photo = UploadedFile::fake()->image($name, 2000, 2000);
        $photo->storeAs('avatars', $name);

        Storage::assertExists('avatars/' . $name);

        $profileController = new ProfileController();
        $profileController->makeThumbnails($name);

        foreach (config('config.user_photo_thumbnails') as $size){
            Storage::assertExists("avatars/{$size}x{$size}/$name");
        }
    }
    public function test_thumbnails_size_is_correct(){
        $name = uniqid() . '.png';
        $this->avatars[] = $name;
        $photo = UploadedFile::fake()->image($name, 2000, 2000);
        $photo->storeAs('avatars', $name);

        $profileController = new ProfileController();
        $profileController->makeThumbnails($name);

        foreach (config('config.user_photo_thumbnails') as $size){
            $img = getimagesize(storage_path("app/avatars/{$size}x{$size}/$name"));
            $this->assertEquals($img[0], $size);
            $this->assertEquals($img[1], $size);
        }
    }
    protected function tearDown(): void
    {
        foreach ($this->avatars as $avatar){
            if(Storage::exists("avatars/$avatar")) {
                Storage::delete("avatars/$avatar");
            }

            foreach (config('config.user_photo_thumbnails') as $size){
                if(Storage::exists("avatars/{$size}x{$size}/$avatar")) {
                    Storage::delete("avatars/{$size}x{$size}/$avatar");
                }
            }
        }

        parent::tearDown();
    }
}
