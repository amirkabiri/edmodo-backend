<?php

namespace Tests\Unit;

use App\Http\Controllers\V1\ProfileController;
use PHPUnit\Framework\TestCase;

class NormalizePhoneAtProfileControllerTest extends TestCase
{
    private $profile;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->profile = new ProfileController();
    }

    public function test_normalizePhone_method_exists_in_ProfileController(){
        $this->assertTrue(method_exists($this->profile, 'normalizePhone'));
    }

    public function test_09_format(){
        $this->assertEquals('+989123456789', $this->profile->normalizePhone('09123456789'));
    }
    public function test_plus_98_format(){
        $this->assertEquals('+989123456789', $this->profile->normalizePhone('+989123456789'));
    }
    public function test_98_format(){
        $this->assertEquals('+989123456789', $this->profile->normalizePhone('989123456789'));
    }
    public function test_9_format(){
        $this->assertEquals('+989123456789', $this->profile->normalizePhone('9123456789'));
    }
}
