<?php

namespace Tests\Unit;

use App\Models\Course;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CourseModelTest extends TestCase
{
    use RefreshDatabase;

    /*
     * methods for creating new user and new course for other test methods using factories
     */
    private function createUser($data = []){
        return factory(User::class)->create($data);
    }
    private function createCourse(User $user = null, $data = []){
        if($user === null) $user = $this->createUser();
        return $user->coursesAsTeacher()->create(factory(Course::class)->make($data)->toArray());
    }

    /*
     * test students_count column is 0 when a course created
     */
    public function test_course_students_count_is_0_at_first_via_factory_make(){
        $course = factory(Course::class)->make();

        $this->assertEquals(0, $course->students_count);
    }
    public function test_course_students_count_is_0_at_first_via_factory_create(){
        $course = $this->createCourse();

        $this->assertEquals(0, $course->students_count);
    }

    /*
     * test students_count column will update when a student join to classs
     */
    public function test_course_students_count_increase_when_student_join_to_course_via_save_method(){
        $course = $this->createCourse();
        $course->members()->save(factory(User::class)->make());

        $course = Course::find($course->id);
        $this->assertEquals(1, $course->students_count);
    }
    public function test_course_students_count_increase_when_student_join_to_course_via_sync_method(){
        $student = $this->createUser();
        $course = $this->createCourse();

        $course->members()->sync([$student->id]);

        $course = Course::find($course->id);
        $this->assertEquals(1, $course->students_count);
    }
    public function test_course_students_count_increase_when_student_join_to_course_via_attach_method(){
        $student = $this->createUser();
        $course = $this->createCourse();

        $course->members()->attach($student->id);

        $course = Course::find($course->id);
        $this->assertEquals(1, $course->students_count);
    }

    /*
     * test students_count column will decrease if a student left from class or delete
     */
    public function test_course_students_count_decrease_when_student_left_from(){
        $student = $this->createUser();
        $course = $this->createCourse();

        $this->assertEquals(0, $course->students_count);

        $course->members()->attach($student->id);
        $course = Course::find($course->id);
        $this->assertEquals(1, $course->students_count);

        $course->members()->detach($student->id);
        $course = Course::find($course->id);
        $this->assertEquals(0, $course->students_count);
    }
    public function test_course_students_count_decrease_when_student_deleted(){
        $student = $this->createUser();
        $course = $this->createCourse();
        $course->students_count = 10;
        $course->save();

        $course->members()->attach($student->id);

        $student->delete();
        $course = Course::find($course->id);
        $this->assertEquals(10, $course->students_count);
    }
}
